# Use Case Name
Use case: technical interview for the DevOps Engineer position at Govtech (Joel's team).
## Overview

The feature overview should answer the following questions:

- Technical interview
- Joel Kek, SWE Govtech, GIG
- Assess candidates worthy to move to round 2 F2F.

## Requirements

You will need the following installed:

- Go to run the application (```go version```)
- Docker for image building/publishing (```docker version```)
- Docker Compose for environment provisioning (```docker-compose version```)
- Git for source control (```git -v```)
- Make for simple convenience scripts (```make -v```)

You will also need the following accounts:

GitLab.com (click [here](http://www.gitlab.com) to register/login)

## Instructions

"Instructions" is usually not the name of the heading.
This is the part of the document where you can include one or more sets of instructions, each to accomplish a specific task.
Headers should describe the task the reader will achieve by following the instructions within, typically starting with a verb.
Larger instruction sets may have subsections covering specific phases of the process.
Where appropriate, provide examples of code or configuration files to better clarify intended usage.

- Write a step-by-step guide, with no gaps between the steps.
- Include example code or configurations as part of the relevant step. Use appropriate markdown to [wrap code blocks with syntax highlighting](../../user/markdown.html#colored-code-and-syntax-highlighting).
- Start with an h2 (`##`), break complex steps into small steps using
subheadings h3 > h4 > h5 > h6. _Never skip a hierarchy level, such
as h2 > h4_, as it will break the TOC and may affect the breadcrumbs.
- Use short and descriptive headings (up to ~50 chars). You can use one
single heading like `## Configuring X` for instructions when the feature
is simple and the document is short.

## Containerization Step
### Create Dockerfile
- Create a file called 'Dockerfile' in deployments/build folder
- Download golang rc-buster image
- Set working directory
- Setup guest VM environment
### Expected output
#### Image created
- Running ```docker build -f ./deployments/build/Dockerfile -t devops/pinger:latest .```
- ```docker images```
- See a new image ```devops/pinger:latest```
#### Functional test
- ```docker run -it -p 8000:8000 devops/pinger:latest``` **same as** ```go run ./cmd/pinger```

## Pipeline Step
### Create .gitlab-ci.yml
 - Specify path to Gitlab project using **variabiles** tag
 - Specify commonly done steps using **anchors** tag
 - Specify your stages **stages** ```test, build_binary, build_image```

## Environment Step
### Create docker-compose file
 - Specify version
 - Specify services

## Documentation Step 
#### This README.md

## Versioning Step
### to be completed


<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->

---

Notes:

- (1): Apply the [tier badges](styleguide.md#product-badges) accordingly
- (2): Apply the correct format for the [GitLab version introducing the feature](styleguide.md#gitlab-versions-and-tiers)
